//
//  DetailViewController.swift
//  TestProject
//
//  Created by qiao on 2020/9/17.
//  Copyright © 2020 qiao. All rights reserved.
//

import UIKit
import SnapKit

class DetailViewController: UITableViewController, RecordManagerDelegate {
    public var isHomePage = true
    public var currentRecord: Record? = nil {
        didSet {
            if let record = self.currentRecord {
                if let json = try? JSONSerialization.jsonObject(with: record.responseData, options: .allowFragments) as? [String: String] {
                    self.jsonData = json
                    self.keys = json.keys.sorted()
                }
                else {
                    self.currentRecord?.sucess = false
                    self.keys = ["failed reason"]
                    self.jsonData = ["failed reason":"Json parse error"]
                }
            }
            self.tableView.reloadData()
        }
    }
    private var keys:[String] = []
    private var jsonData:[String: String] = [:]
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isHomePage {
            RecordManager.shared.delegate = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UINib(nibName: "StatusTableViewCell", bundle: nil), forCellReuseIdentifier: "StatusTableViewCell")
        self.title = "Detail"
        if isHomePage {
            self.currentRecord = RecordManager.shared.recordList.first
            let barItem = UIBarButtonItem(title: "History", style: .plain, target: self, action: #selector(openHistory))
            self.navigationItem.rightBarButtonItem = barItem
        }
    }

    @objc func openHistory() {
        let history = HistoryListViewController()
        self.navigationController?.pushViewController(history, animated: true)
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1 + self.keys.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            if let record = self.currentRecord,
                let cell = tableView.dequeueReusableCell(withIdentifier: "StatusTableViewCell") as? StatusTableViewCell {
                cell.updateCell(record)
                return cell
            }
            else {
                let cell = UITableViewCell(style: .default, reuseIdentifier: "loadingCell")
                cell.textLabel?.text = "loading..."
                cell.textLabel?.textAlignment = .center
                cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 24)
                let height = tableView.frame.size.height
                cell.textLabel?.snp.makeConstraints({ (make) in
                    make.edges.equalToSuperview()
                    make.height.equalTo(height)
                })
                return cell
            }
        }
        else {
            let row = indexPath.row - 1
            let cell = (tableView.dequeueReusableCell(withIdentifier: "rowcell")) ?? UITableViewCell(style: .subtitle, reuseIdentifier: "rowcell")
            cell.selectionStyle = .none
            let key = self.keys.count > row ? self.keys[row] : nilString
            cell.textLabel?.text = key
            cell.detailTextLabel?.text = self.jsonData[key] ?? nilString
            cell.detailTextLabel?.numberOfLines = 0
            return cell
        }
    }
    
    func recordListChanged(_ changeList:[Record], refreshTop: Bool) {
        self.currentRecord = RecordManager.shared.recordList.first
    }
}
