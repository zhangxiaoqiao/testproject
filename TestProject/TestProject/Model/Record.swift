//
//  Record.swift
//  TestProject
//
//  Created by qiao on 2020/9/17.
//  Copyright © 2020 qiao. All rights reserved.
//

import UIKit
import SQLite

public let nilString = "--"

struct Record {
    let timeStamp: Double
    var sucess: Bool
    let responseData: Data
    static let dateformat = DateFormatter()
    init(sucess: Bool, responseData: Data) {
        self.timeStamp = Date().timeIntervalSince1970
        self.sucess = sucess
        self.responseData = responseData
    }
    
    init(timeStamp: Double, sucess: Bool, responseData: Data) {
        self.timeStamp = timeStamp
        self.sucess = sucess
        self.responseData = responseData
    }
    
    public func detailTimeText() -> String {
        Record.dateformat.dateFormat = "yyyy-MM-dd \n HH:mm:ss"
        let date = Date(timeIntervalSince1970: self.timeStamp)
        return Record.dateformat.string(from: date)
    }
    
    public func listTimeText() -> String {
        Record.dateformat.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = Date(timeIntervalSince1970: self.timeStamp)
        return Record.dateformat.string(from: date)
    }
}

struct RecordColumn {
    static let timeStamp = Expression<Double>("timeStamp")
    static let sucess = Expression<Bool>("sucess")
    static let responseText = Expression<Data>("responseText")
    
    static func insert(record: Record, table: Table) -> Insert {
        return table.insert(or: .replace, [
            self.timeStamp <- record.timeStamp,
            self.sucess <- record.sucess,
            self.responseText <- record.responseData
            ]
        )
    }
    
    static func mapper(_ row: Row) -> Record? {
        guard let timeStamp = try? row.get(self.timeStamp),
            let sucess = try? row.get(self.sucess),
            let responseText = try? row.get(self.responseText) else {
                return nil
        }
        return Record(timeStamp: timeStamp,
                      sucess: sucess,
                      responseData: responseText)
    }
        
    static func create(_ table: Table) -> String {
        return table.create(ifNotExists: true, block: { builder in
            builder.column(RecordColumn.timeStamp, primaryKey: true)
            builder.column(RecordColumn.sucess)
            builder.column(RecordColumn.responseText)
        })
    }
}
