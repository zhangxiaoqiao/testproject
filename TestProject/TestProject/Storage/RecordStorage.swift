//
//  RecordStorage.swift
//  TestProject
//
//  Created by qiao on 2020/9/17.
//  Copyright © 2020 qiao. All rights reserved.
//

import UIKit
import SQLite

class RecordStorage {
    public static let shared = RecordStorage()
    
    private var table: Table = Table("HttpRecords")
    private var db: Connection!
    
    public var recordList:[Record] = []
    
    private init() {
        let document = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let dbpath = document + "/db.sqlite3"
        let dbExists = FileManager.default.fileExists(atPath: dbpath)
        do {
            db = try Connection(dbpath)
            db.busyTimeout = 3
        } catch {
            print("数据库创建失败！")
        }
        if !dbExists {
            do {
                _ = try db.run(RecordColumn.create(self.table))
            } catch {
                print("table创建失败！")
            }
        }
    }
    
    public func insert(_ record: Record) {
        do {
            _ = try db.run(RecordColumn.insert(record: record, table: self.table))
        } catch {
            print("数据库插入失败\(record.timeStamp)！")
        }
    }
    
    public func totalCount() -> Int {
        let query = self.table.count
        if let count = try? db.scalar(query) {
            return count
        }
        return 0 
    }
    
    public func query(_ offset: Int = 0, count: Int = 100) -> [Record] {
        let query = self.table.order(RecordColumn.timeStamp.desc).limit(count, offset: offset)
        if let list: [Record] = try? db.prepare(query).compactMap({ RecordColumn.mapper($0) }) {
            return list
        }
        return []
    }
    
    public func deleteAll() {
        let delete = self.table.delete()
        if let count = try? db.run(delete) {
            print("删除的条数为：\(count)")
        } else {
            print("删除失败")
        }
    }
}
