//
//  UIColor+Ex.swift
//  TestProject
//
//  Created by qiao on 2020/9/18.
//  Copyright © 2020 qiao. All rights reserved.
//

import UIKit

extension UIColor {
    public convenience init(_ rgba: UInt32) {
        self.init(red: CGFloat(Int(rgba >> 16) & 0xFF) / 255.0,
                  green: CGFloat(Int(rgba >> 8) & 0xFF) / 255.0,
                  blue: CGFloat(Int(rgba) & 0xFF) / 255.0,
                  alpha: CGFloat(1))
    }
}
