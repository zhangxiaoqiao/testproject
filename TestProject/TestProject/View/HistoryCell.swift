//
//  HistoryCell.swift
//  TestProject
//
//  Created by qiao on 2020/9/17.
//  Copyright © 2020 qiao. All rights reserved.
//

import UIKit

class HistoryCell: UITableViewCell {

    @IBOutlet var statusLabel: UILabel? = nil
    @IBOutlet var timeLabel: UILabel? = nil
    @IBOutlet var detailLabel: UILabel? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func updateCell(_ record: Record) {
        if record.sucess {
            self.statusLabel?.textColor = UIColor(0x0EC28C)
            self.statusLabel?.text = "Sucess"
        }
        else {
            self.statusLabel?.textColor = UIColor(0xFF526D)
            self.statusLabel?.text = "Failed"
        }
        self.timeLabel?.text = record.listTimeText()
        self.detailLabel?.text = String(data: record.responseData, encoding: .utf8) ?? nilString
    }
}
