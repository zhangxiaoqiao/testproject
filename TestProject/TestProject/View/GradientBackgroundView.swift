//
//  GradientBackgroundView.swift
//  TestProject
//
//  Created by qiao on 2020/9/18.
//  Copyright © 2020 qiao. All rights reserved.
//

import UIKit

extension UIView {
    public enum GradientDirectionOption {
        case topToBottom
        case leftToRight
        case topLeftToBottomRight
        case topRightToBottomLeft
    }
    
    /// 设置背景为渐变色
    public func applyGradientColor(_ colors: [UIColor],
                                   alpha: CGFloat? = nil,
                                   option: UIView.GradientDirectionOption = .leftToRight) {
        if let view = self.subviews.compactMap({ $0 as? GradientBackgroundView }).first {
            view.config(colors, alpha: alpha, option: option)
        } else {
            let view = GradientBackgroundView()
            self.insertSubview(view, at: 0)
            view.snp.makeConstraints { (make) in
                make.edges.equalToSuperview()
            }
            view.config(colors, alpha: alpha, option: option)
        }
    }
    
    public func removeGradientColor() {
        self.subviews.forEach({ ($0 as? GradientBackgroundView)?.removeFromSuperview() })
    }
}

public class GradientBackgroundView: UIView {
    let gradientLayer = CAGradientLayer()
    
    init() {
        super.init(frame: .zero)
        
        self.backgroundColor = .clear
        self.isUserInteractionEnabled = false
        
        self.gradientLayer.backgroundColor = UIColor.clear.cgColor
        
        self.layer.addSublayer(self.gradientLayer)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func config(_ colors: [UIColor], alpha: CGFloat? = nil, option: UIView.GradientDirectionOption = .leftToRight) {
        
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        switch option {
        case .leftToRight:
            self.gradientLayer.startPoint = CGPoint(x: 0, y: 0.5)
            self.gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        case .topToBottom:
            self.gradientLayer.startPoint = CGPoint(x: 0.5, y: 0)
            self.gradientLayer.endPoint = CGPoint(x: 0.5, y: 1.0)
        case .topLeftToBottomRight:
            self.gradientLayer.startPoint = CGPoint(x: 0, y: 0)
            self.gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
        case .topRightToBottomLeft:
            self.gradientLayer.startPoint = CGPoint(x: 0, y: 1.0)
            self.gradientLayer.endPoint = CGPoint(x: 0, y: 1.0)
        }
        if let alpha = alpha {
            self.gradientLayer.colors = colors.map({ $0.withAlphaComponent(alpha).cgColor })
        } else {
            self.gradientLayer.colors = colors.map({ $0.cgColor })
        }
        CATransaction.commit()
    }
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        self.gradientLayer.frame = self.bounds
        CATransaction.commit()
    }
}

