//
//  StatusTableViewCell.swift
//  TestProject
//
//  Created by qiao on 2020/9/17.
//  Copyright © 2020 qiao. All rights reserved.
//

import UIKit



class StatusTableViewCell: UITableViewCell {
    @IBOutlet var largeBgview: UIView? = nil
    @IBOutlet var circleBgview: UIView? = nil
    @IBOutlet var statusLabel: UILabel? = nil
    @IBOutlet var timeLabel: UILabel? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.largeBgview?.layer.cornerRadius = 48
        self.circleBgview?.layer.cornerRadius = 44
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func updateCell(_ record: Record) {
        if record.sucess {
            self.largeBgview?.backgroundColor = UIColor(0x0EC28C).withAlphaComponent(0.4)
            self.circleBgview?.backgroundColor = UIColor(0x0EC28C)
            self.statusLabel?.text = "Sucess"
        }
        else {
            self.largeBgview?.backgroundColor = UIColor(0xFF526D).withAlphaComponent(0.4)
            self.circleBgview?.backgroundColor = UIColor(0xFF526D)
            self.statusLabel?.text = "Failed"
        }
        self.timeLabel?.text = record.detailTimeText()
    }
}
