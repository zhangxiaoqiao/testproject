//
//  RecordManager.swift
//  TestProject
//
//  Created by qiao on 2020/9/17.
//  Copyright © 2020 qiao. All rights reserved.
//

import UIKit
import Alamofire

protocol RecordManagerDelegate {
    func recordListChanged(_ changeList:[Record], refreshTop: Bool)
}

class RecordManager: NSObject {
    public static let shared = RecordManager()
    private(set) var recordList:[Record] = []
    public var delegate: RecordManagerDelegate? = nil
    
    private var timer: Timer?
    private override init() {
        super.init()
        AF.sessionConfiguration.timeoutIntervalForRequest = 5
        
        self.loadMore()
        self.startTimer()
    }
    
    func startTimer() {
        let timer = Timer.scheduledTimer(withTimeInterval: 5, repeats: true) { [weak self] (timer) in
            guard let `self` = self else { return }
            self.refreshData()
        }
        RunLoop.main.add(timer, forMode: .common)
        self.timer = timer
    }
    
    public func invalidateTimer() {
        self.timer?.invalidate()
        self.timer = nil
    }
    
    public func manualRefreshData() {
        self.invalidateTimer()
        self.refreshData()
    }
    
    private func refreshData() {
        let url = "https://api.github.com/"
        AF.request(url).responseData { (response) in
            switch response.result {
            case .success(let value):
                let record = Record(sucess: true, responseData: value)
                self.saveRecord(record)
            case .failure(let error):
                let text = "{\"failed reason\":\"\(error.errorDescription ?? nilString)\"}"
                let data = text.data(using: .utf8) ?? Data()
                let record = Record(sucess: false, responseData: data)
                self.saveRecord(record)
                break;
            }
            if self.timer == nil {
                self.startTimer()
            }
        }
    }
    
    public func loadMore(_ count: Int = 100) {
        let offset = self.recordList.count
        let records = RecordStorage.shared.query(offset, count: count)
        self.recordList.append(contentsOf: records)
        self.delegate?.recordListChanged(records, refreshTop: false)
    }
    
    public func hasMore() -> Bool {
        let count = RecordStorage.shared.totalCount()
        return count > self.recordList.count
    }
    
    private func saveRecord(_ record: Record) {
        if self.recordList.isEmpty {
            self.recordList.append(record)
        }
        else {
            self.recordList.insert(record, at: 0)
        }
        RecordStorage.shared.insert(record)
        self.delegate?.recordListChanged([record], refreshTop: true)
    }
}
