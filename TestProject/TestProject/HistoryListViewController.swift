//
//  HistoryListViewController.swift
//  TestProject
//
//  Created by qiao on 2020/9/17.
//  Copyright © 2020 qiao. All rights reserved.
//

import UIKit
import MJRefresh
import MBProgressHUD
import Foundation

class HistoryListViewController: UITableViewController, RecordManagerDelegate {
    private var newestRecord: Record? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "History List"
        self.tableView.register(UINib(nibName: "HistoryCell", bundle: nil), forCellReuseIdentifier: "HistoryCell")
        self.tableView.mj_header = MJRefreshNormalHeader.init(refreshingBlock: {
            RecordManager.shared.manualRefreshData()
        })
        if RecordManager.shared.hasMore() {
            self.tableView.mj_footer = MJRefreshAutoNormalFooter.init(refreshingBlock: {
                RecordManager.shared.loadMore()
            })
        }
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        RecordManager.shared.delegate = self
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return RecordManager.shared.recordList.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryCell", for: indexPath)
        if let historyCell = cell as? HistoryCell,
            indexPath.row < RecordManager.shared.recordList.count {
            let record = RecordManager.shared.recordList[indexPath.row]
            historyCell.updateCell(record)
            if let newest = self.newestRecord,
                abs(newest.timeStamp - record.timeStamp) < 0.0000000001 {
                let sucessColors = [UIColor(0x00C288).withAlphaComponent(0.3),
                                   UIColor(0x4BD587).withAlphaComponent(0.3)]
                let failedColors = [UIColor(0xFF526D).withAlphaComponent(0.3),
                                    UIColor(0xFF6B59).withAlphaComponent(0.3)]
                
                historyCell.contentView.applyGradientColor(record.sucess ? sucessColors : failedColors)
            }
            else {
                historyCell.contentView.removeGradientColor()
            }
            
        }
        
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: false)
        if  indexPath.row < RecordManager.shared.recordList.count {
            let record = RecordManager.shared.recordList[indexPath.row]
            let detail = DetailViewController()
            detail.isHomePage = false
            detail.currentRecord = record
            self.navigationController?.pushViewController(detail, animated: true)
        }
    }
    
    func recordListChanged(_ changeList:[Record], refreshTop: Bool) {
        if refreshTop {
            self.newestRecord = changeList.first
            self.tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .fade)
            if RecordManager.shared.recordList.count >= 2 {
                let cell = self.tableView.cellForRow(at: IndexPath(row: 1, section: 0))
                cell?.contentView.removeGradientColor()
            }
            self.tableView.mj_header?.endRefreshing()
            // 当前视图可见才弹toast
            if self == self.navigationController?.viewControllers.last {
                self.showToast("收到一条新的调用记录")
            }
        }
        else {
            self.tableView.reloadData()
            self.tableView.mj_footer?.endRefreshing()
            if !RecordManager.shared.hasMore() {
                self.tableView.mj_footer = nil
            }
        }
    }
    
    private func showToast(_ text: String) {
        let hud = self.hud()
        hud.mode = .text
        hud.label.text = text
        self.view.bringSubviewToFront(hud)
        hud.show(animated: true)
        hud.isUserInteractionEnabled = false
        let time = DispatchTime.now() + .milliseconds(Int(2 * 1_000))
        DispatchQueue.main.asyncAfter(deadline: time) {
            hud.hide(animated: true)
        }
    }
    
    private func hud(isClear: Bool = false) -> MBProgressHUD {
        let parentView: UIView = UIApplication.shared.windows.first ?? self.view
        var hud: MBProgressHUD!
        if let h = MBProgressHUD.forView(parentView) {
            hud = h
        } else {
            hud = MBProgressHUD.showAdded(to: parentView, animated: true)
            hud.bezelView.style = .solidColor
            hud.minShowTime = 0.5
            hud.animationType = .fade
            hud.label.font = UIFont.systemFont(ofSize: 14)
            hud.detailsLabel.font = UIFont.systemFont(ofSize: 14)
            hud.label.numberOfLines = 0
            hud.detailsLabel.numberOfLines = 0
            if isClear {
                hud.bezelView.backgroundColor = UIColor.clear
                hud.contentColor = UIColor.clear
            } else {
                hud.bezelView.backgroundColor = UIColor.black.withAlphaComponent(0.66)
                hud.contentColor = UIColor.white
            }
        }
        return hud
    }
}
