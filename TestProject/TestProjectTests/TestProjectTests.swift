//
//  TestProjectTests.swift
//  TestProjectTests
//
//  Created by qiao on 2020/9/17.
//  Copyright © 2020 qiao. All rights reserved.
//

import XCTest
@testable import TestProject

class TestProjectTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        RecordStorage.shared.deleteAll()
        let data = "{\"current_user_url\":\"https://api.github.com/user\",\"current_user_authorizations_html_url\":\"https://github.com/settings/connections/applications{/client_id}\"}".data(using: .utf8) ?? Data()
        RecordStorage.shared.insert(Record(timeStamp: Date().timeIntervalSince1970, sucess: true, responseData: data))
        RecordStorage.shared.insert(Record(timeStamp: 0, sucess: true, responseData: "test123".data(using: .utf8) ?? Data()))
        RecordStorage.shared.insert(Record(timeStamp: 0, sucess: false, responseData: "test124".data(using: .utf8) ?? Data()))
        RecordStorage.shared.insert(Record(timeStamp: -1, sucess: false, responseData: "abc".data(using: .utf8) ?? Data()))
        let records = RecordStorage.shared.query()
        XCTAssert(records.count == 3)
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
